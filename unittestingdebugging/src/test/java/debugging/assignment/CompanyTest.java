package debugging.assignment;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test Company class
 */
public class CompanyTest
{
    /**
     * Checks for different employees
     */
    @Test
    public void testDifferentEmployees()
    {
        Employee[] employees = new Employee[] { new Employee("Dan", 12345), new Employee("Gabriela", 123), new Employee("Andrew", 12)};

        Company c1 = new Company(employees);

        // change it so that it's a different company

        employees[0].setEmployeeId(12);

        Company c2 = new Company(employees);
        // for (int i=0; i<employees.length;i++)
        // {
        //     System.out.println(c1.getEmployee(i).getEmployeeId());
        //     System.out.println(c2.getEmployee(i).getEmployeeId());
        
        // }
        assertNotEquals(c1, c2);
    }
}
